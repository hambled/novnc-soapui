
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[[ -x /usr/bin/lesspipe ]] &&
    eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
[[ -z $debian_chroot ]] && [[ -r /etc/debian_chroot ]] &&
    debian_chroot=$(cat /etc/debian_chroot)

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm|xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

[[ -n $force_color_prompt ]] && {
    if [[ -x /usr/bin/tput ]] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
}

function timer_start {
  BASH_PS1_TIMER=${BASH_PS1_TIMER:-$SECONDS}
}

function timer_stop {
  timer_show=$(($SECONDS - $BASH_PS1_TIMER))
  unset BASH_PS1_TIMER
}

trap 'timer_start' DEBUG

if [[ -z $PROMPT_COMMAND ]]; then
  PROMPT_COMMAND="timer_stop"
else
  PROMPT_COMMAND="$PROMPT_COMMAND; timer_stop"
fi


if [[ "$color_prompt" = yes ]]; then
    YoG=$([[ $EUID -eq 0 ]] && echo -n "\[\033[1;33m\]" || echo -n "\[\033[0;33m\]")
    PS1_BANNER=Docker_McA
    NONE='\[\033[0;0m\]'
    PS1="$YoG[$NONE\${timer_show}s$YoG]$NONE"
    PS1+='$(E=$?;[[ $E -ne 0 ]] && echo "\[\033[1;31m\]$E\342\206\265'"$NONE\")"
    PS1+='${PS1_BANNER:+\[\033[1;5;31m\][$PS1_BANNER]\[\033[0;0;0m\]\n}'
    PS1+='$(d=$(dirs -v|wc -l);[[ $d -gt 1 ]] && echo "\[\033[36m\][$(($d-1))]\[\033[0m\]")'
    PS1+="$YoG[\[\033[1;35m\]\w$NONE$YoG]$NONE"
    PS1+="$YoG\$$NONE"
    PS1+='$([[ -n "$HISTFILE" ]] || echo -n "\[\033[0;32m\]\742\600\640'"$NONE\") "
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h \w \$ '
fi
unset color_prompt force_color_prompt

# enable color support of ls and also add handy aliases
[[ -x /usr/bin/dircolors ]] && {
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
}

man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    man "$@"
}

alias    egrep-grey="GREP_COLOR='1;30' egrep --color=always"
alias     egrep-red="GREP_COLOR='1;31' egrep --color=always"
alias   egrep-green="GREP_COLOR='1;32' egrep --color=always"
alias  egrep-yellow="GREP_COLOR='1;33' egrep --color=always"
alias    egrep-blue="GREP_COLOR='1;34' egrep --color=always"
alias egrep-magenta="GREP_COLOR='1;35' egrep --color=always"
alias    egrep-cyan="GREP_COLOR='1;36' egrep --color=always"
alias   egrep-white="GREP_COLOR='1;37' egrep --color=always"

# date_ [<filtro> [<parametro1> ...]]
# v1.0
date_(){
    local zones="$(find /usr/share/zoneinfo | sed 's#/usr/share/zoneinfo/##' | grep -i "${1:-.}" | sort | uniq)"
    local n=1
    local m
    shift

    [ -z "$zones" ] && return 1
    while read i; do
        echo -e "\e[36m[$n]\e[0m" $i
        n=$(($n + 1))
    done < <(echo "$zones")
    n=$(echo "$zones" | wc -l)
    read -p $'\033'"[32mHora de:"$'\033'"[0m " m
    [ -z "$m" ] && return
    [ $m -gt 0 -a $m -le $n ] && {
        local tz=$(echo "$zones" | head -n$m | tail -n1)
        TZ=$tz date $@
    }
}

# Pasar a segundos.
# toSec [{<num>[dDhHmMsS]}...]
# Ej:
#   toSec 1d1h1m1
#   90061
# v1.0
toSec(){
    local s=${1,,}s
    s=${s//d/*24h}
    s=${s//h/*60m}
    s=${s//m/*60s}
    s=${s//s/+}
    echo $((${s}0))
}

# Countdown.
# Bug: No se visualizan los días cuando se pasa de 24h.
# Dependencia: función "toSec".
# v1.0
countdown(){
    local end
    end=$(toSec "$1") || return 1
    end=$(($(date +%s) + $end))
    date
    for (( i=$(date +%s); i < $end; i=$(date +%s) )); do
        echo -ne "$(date -u --date @$(($end - $i)) +%T)\r";
        sleep 1
    done
    date
    echo -e '\007'
}


# Uncomment file. (Default comment character := '#').
# uc <file> [<comment character>]
# uc        [<comment character>] < <random command>
uc(){
    local f
    [[ -e $1 && ! -d $1 ]] && f="$1" && shift
    local c="${1:-#}"
    if [[ -n "$f" ]]; then
        egrep -v "^\s*($c|$)" "$f"
    else
        egrep -v "^\s*($c|$)" <&0
    fi
}

alias ,='pushd .'
alias ,-='pushd -'
alias .-='cd -'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'
alias ........='cd ../../../../../../..'
alias .........='cd ../../../../../../../..'
alias ..........='cd ../../../../../../../../..'
alias ...........='cd ../../../../../../../../../..'
alias ............='cd ../../../../../../../../../../..'
alias d='dirs -v | tail -n+2'
alias 0='popd >/dev/null; dirs -v | tail -n+2'
alias 1='pushd +1'
alias 2='pushd +2'
alias 3='pushd +3'
alias 4='pushd +4'
alias 5='pushd +5'
alias 6='pushd +6'
alias 7='pushd +7'
alias 8='pushd +8'
alias 9='pushd +9'

# Explorador de archivos con fuzzing.
# tab       : Permite marcar más de una carpeta.
# ctrl-space: Almacena las rutas a las carpetas marcadas.
# ctrl-r    : Regresa a la ruta inicial.
# ctrl-h    : Regresa al home.
# ctrl-d    : Ir a las rutas guardadas.
# ctrl-f    : Usa el comando find para mostrar subdirectorios.
# ctrl-g    : Como ctrl-f pero muestra también ficheros.
# ctrl-c    : Lista los ficheros y links y muestra en la preview el seleccionado.
# ctrl-p    : Imprime los elementos seleccionados.
# ctrl-q    : Salir.
ccd(){
    local version=1.9.3[MOD]
    command -v fzf &> /dev/null || { cd "$1"; return 1; }

    local folders
    local fzf_ret
    local push_me
    local query
    local ccd_command
    local oldoldpwd="$OLDPWD"
    local oldpwd="$PWD"
    local bind
    local dirs
    local find
    local cat
    local window_size

    bind+='enter:execute(echo cd)+accept,'
    bind+='ctrl-space:execute(echo pushd)+accept,'
    bind+='ctrl-r:execute(echo reset)+accept,'
    bind+='ctrl-h:execute(echo home)+accept,'
    bind+='ctrl-d:execute(echo dirs)+accept,'
    bind+='ctrl-f:execute(echo find)+accept,'
    bind+='ctrl-g:execute(echo find-files)+accept,'
    bind+='ctrl-c:execute(echo cat)+accept,'
    bind+='ctrl-p:execute(echo print)+accept'

    [[ -d $1 ]] && cd "$1"
    while
        fzf_ret=$(
            [[ -n $find ]] && window_size=50
            [[ -n $find ]] || window_size=70
            {
                echo -e ..\\n.
                case "$find" in
                    1) find -type d 2>/dev/null | sed s%^\\.\\?/%%g;;
                    2) find         2>/dev/null | sed s%^\\.\\?/%%g;;
                    *) compgen -d | LC_ALL=C sort -fr;;
                esac
            } |
            fzf ${query:+-q "$query"} \
                --cycle       \
                --multi       \
                --print-query \
                --bind="$bind"\
                --preview='echo "Current path: $PWD";echo "Files path: ${PWD%/}/"{};echo;ls -lahHF --color=always {}' \
                --preview-window=${window_size}%
        )
    do
        # fzf_ret := {ccd_command, '\n', query, '\n', folders}
        unset query
        unset find
        ccd_command="${fzf_ret%%$'\n'*}"
        folders="${fzf_ret#*$'\n'*$'\n'}"

        case "$ccd_command" in
        cd) # Enter
            folders="./${folders##*$'\n'}"
            [ -d "${folders}" ] && cd "$folders" || cd "${folders%/*}"
        ;;
        pushd) # ctrl-space
            query="${fzf_ret#*$'\n'}"
            query="${query%%$'\n'*}"
            while read push_me; do
                pushd -n "${PWD%/}/$push_me" > /dev/null
            done < <(echo "$folders")
        ;;
        reset) # ctrl-r
            cd "$oldpwd"
        ;;
        home) # ctrl-h
            cd
        ;;
        dirs) # ctrl-d
            dirs=$(dirs -v | tac | fzf | sed 's/^ *//' | cut -d\  -f1)
            [[ -n "$dirs" ]] && pushd +$dirs > /dev/null
        ;;
        find) # ctrl-f
            query="${fzf_ret#*$'\n'}"
            query="${query%%$'\n'*}"
            find=1
        ;;
        find-files) # ctrl-g
            query="${fzf_ret#*$'\n'}"
            query="${query%%$'\n'*}"
            find=2
        ;;
        cat) # ctrl-c
            local pv
            pv='[ $(stat -c "%s" {}) -gt $((10*1024*1024)) ] && '
            pv+='echo "ERROR: File to big..." || { '
            pv+='  file -L {} 2>&1 | egrep -q -i "text|json" && '
            pv+='  cat {} || {'
            pv+='    echo -n "ERROR: It is not a text file: ";file -b {};};}'
            find -maxdepth 1 -type f -or -type l |\
                sed 's#^.\?/\?##'                |\
                fzf                               \
                --cycle                           \
                --bind='pgdn:preview-page-down,pgup:preview-page-up' \
                --preview="$pv"       \
                --preview-window=80%
        ;;
        print) # ctrl-p
            echo "$folders"
        ;;
        esac
    done
    # OLDPWD it's a bash variable used for example when you execute the command 'cd -'.
    [[ "$PWD" == "$oldpwd" ]] && OLDPWD="$oldoldpwd" || OLDPWD="$oldpwd"
}

hf(){
    local version=1.1
    command -v fzf &> /dev/null || { history; return 1; }
    history | fzf -m +s --tac --no-mouse | sed 's/^ *[0-9]* \+//'
}
# Este alias está pensado para ser usado con la combinación de teclas C-M-e.
alias hf_='$(hf | tr \\n \; | sed s/\;\$//)'

nanox(){
    [[ ! -e $1 ]] && echo "#!/bin/bash" >> "$1"
    nano "$1"
    [[ -f $1 ]] && [[ ! -L $1 ]] && chmod u+x "$1"
}

txzc(){
    [[ -z $1 ]] && return 1
    local pv
    command -v pv >/dev/null && pv="pv -s $(du -bs "$1" | cut -f1)"
    tar c "$1" | ${pv:- cat -} | xz -c > "${1%/}.txz"
    xz -l "${1%/}.txz"
}

txzx(){
    [[ -z $1 ]] && return 1
    local pv
    command -v pv >/dev/null && pv=pv
    ${pv:-cat} "$1" | tar xJ
}

recv(){
    local ip=${1:-127.0.0.1}
    local port=${2:-2468}
    [[ $ip =~ ^[.0-9]*$ ]] || {
    >&2 echo 'IP no valida.'
        exit 1
    }

    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        exit 1
    }
    ncat --recv-only $ip $port
}

recvl(){
    local port=${1:-2468}
    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        exit 1
    }
    ncat --recv-only -vl $port
}
send(){
    local ip=${1:-127.0.0.1}
    local port=${2:-2468}
    [[ $ip =~ ^[.0-9]*$ ]] || {
    >&2 echo 'IP no valida.'
        exit 1
    }

    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        exit 1
    }
    ncat --send-only $ip $port <&0
}

sendl(){
    local port=${1:-2468}
    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        exit 1
    }
    ncat --send-only -vl $port <&0
}

up_="{ h=\$(history | sed -n '\${s/ *[0-9]\+ *\(.*\) *up_ *\$/\1/p}'); "
up_+='c=$(up -o /dev/stdout 2>/dev/null | sed 1d;); [[ -n $c ]] && echo "$h$c"; } && '
up_+='history -d-1'
alias up_="$up_"
unset up_

mv_(){
    local f ff
    f=$(fzf)
    [[ -n $f ]] && {
        read -ep "dst: " -i "$f" ff
        mv $f "${ff:-$f}"
    }
}

alias bell="echo -ne '\007'"


alias rgrep='rgrep --color=always'

alias ip_='echo -e "Public ip: \e[1;35m$(wget -T2 -qO- ipinfo.io/ip)\e[0;0m";ip -4 -o addr show;ip route show'
alias u='unset HISTFILE'

alias ll='ls -lahF'

source <(yq shell-completion)
alias yq='yq -C'
alias yq='yq -C r -'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

[[ -f ~/.bash_aliases ]] &&
    . ~/.bash_aliases

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
! shopt -oq posix && {
    if [[ -f /usr/share/bash-completion/bash_completion ]]; then
        . /usr/share/bash-completion/bash_completion
    elif [[ -f /etc/bash_completion ]]; then
        . /etc/bash_completion
    fi
}

[[ -d $HOME/bin ]] && ! echo $PATH | grep -q "$HOME/bin" &&
    export PATH=$HOME/bin:$PATH

shopt -s histverify
shopt -s autocd
