FROM golang AS easy-novnc-build
WORKDIR /src
RUN go mod init build && \
    go get github.com/geek1011/easy-novnc@v1.1.0 && \
    go build -o /bin/easy-novnc github.com/geek1011/easy-novnc


FROM debian:buster AS easy-novnc-empty

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        openbox                    \
        tigervnc-standalone-server \
        supervisor                 \
        gosu                       \
        htop                       \
        lxterminal                 \
        ca-certificates         && \
    mkdir -p /usr/share/desktop-directories

COPY --from=easy-novnc-build /bin/easy-novnc /usr/local/bin/
COPY menu-default.xml /etc/xdg/openbox/menu.xml
COPY supervisord.conf /etc/
EXPOSE 8080

RUN groupadd --gid 1000 app && \
    useradd --home-dir /data --shell /bin/bash --uid 1000 --gid 1000 app && \
    mkdir -p /data
VOLUME /data

CMD ["sh", "-c", "chown app:app /data /dev/stdout && exec gosu app supervisord"]


FROM easy-novnc-empty AS mca-custom

RUN apt-get update -y && \
    apt-get install -y  \
        bsdmainutils    \
        dnsutils        \
        xz-utils        \
        jq              \
        fzf             \
        git             \
        bash            \
        bash-completion \
        nano            \
        file            \
        tree            \
        zstd            \
        wget            \
        curl
COPY ./.bashrc /root/.bashrc

RUN wget -qO /bin/up https://github.com/akavel/up/releases/download/v0.3.2/up && chmod +x /bin/up
RUN wget -qO /bin/yq https://github.com/mikefarah/yq/releases/download/3.3.2/yq_linux_amd64 && chmod +x /bin/yq


FROM mca-custom AS soap-gui
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        default-jre && \
    rm -rf /var/lib/apt/lists

RUN wget -qO SoapUI-x64-5.6.0.sh https://s3.amazonaws.com/downloads.eviware/soapuios/5.6.0/SoapUI-x64-5.6.0.sh
RUN chmod +x /SoapUI-x64-5.6.0.sh
RUN yes '' | /SoapUI-x64-5.6.0.sh
RUN rm /SoapUI-x64-5.6.0.sh

COPY menu-soapui.xml /etc/xdg/openbox/menu.xml
