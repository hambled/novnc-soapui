# Aplicaciones GUI en Docker

Este proyecto es una versión simplificada y personalizada de lo explicado en [este post][Post original].

Este proyecto permite encapsular una app (o varias) en un contenedor.  
Se puede acceder a esta app mediante cualquier navegador gracias al uso de No-VNC y Openbox.

> **¡Importante!**: Esta versión no incluye seguridad *TLS*, por lo que no es recomendable usarla desde fuera del propio host.
> Si se quiere más seguridad, mirar el [post original][Post original].

## SoapUI
En este caso se ha encapsulado la app de **SoapUI**.

## Uso
```bash
# Crear la imagen:
docker build -t soapui .

# Arrancar el contenedor:
docker run --detach --name soapui soapui

# Arrancar el contenedor en "modo usar y tirar":
docker run --rm -ti --name soapui soapui

# Coger la ip para poder ver la app en el navegador:
docker exec soapui hostname -i

# Entrar en el contenedor si se desea instalar algo más:
docker exec -ti soapui bash

# Parar el contenedor:
docker stop soapui

# Arrancar de nuevo:
docker start soapui

# Borrar:
docker rm soapui
```

Ahora en el navegador metemos la ip del contenedor y el puerto 8080

<!-- URL -->

[Post original]: https://www.digitalocean.com/community/tutorials/how-to-remotely-access-gui-applications-using-docker-and-caddy-on-ubuntu-20-04-es "Post original de Digital Ocean"